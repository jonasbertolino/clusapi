<?php

/* CoreBundle:Index:index.html.twig */
class __TwigTemplate_626b945f94ac8b27a29b986c617ed48c59f675d755a6867cf5281d9c300ede9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "CoreBundle:Index:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fad77ba42a5f667bec1d06c9f21de403dbc98a5f629fd23f71b3732ba12f2af3 = $this->env->getExtension("native_profiler");
        $__internal_fad77ba42a5f667bec1d06c9f21de403dbc98a5f629fd23f71b3732ba12f2af3->enter($__internal_fad77ba42a5f667bec1d06c9f21de403dbc98a5f629fd23f71b3732ba12f2af3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CoreBundle:Index:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fad77ba42a5f667bec1d06c9f21de403dbc98a5f629fd23f71b3732ba12f2af3->leave($__internal_fad77ba42a5f667bec1d06c9f21de403dbc98a5f629fd23f71b3732ba12f2af3_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_1ed7eff7e16bb5ce0390fd8464ea016e0c775285b2abfb28787e1bbc0cf80da5 = $this->env->getExtension("native_profiler");
        $__internal_1ed7eff7e16bb5ce0390fd8464ea016e0c775285b2abfb28787e1bbc0cf80da5->enter($__internal_1ed7eff7e16bb5ce0390fd8464ea016e0c775285b2abfb28787e1bbc0cf80da5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Clube Saudosista de Piracicaba";
        
        $__internal_1ed7eff7e16bb5ce0390fd8464ea016e0c775285b2abfb28787e1bbc0cf80da5->leave($__internal_1ed7eff7e16bb5ce0390fd8464ea016e0c775285b2abfb28787e1bbc0cf80da5_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_9535dc597c8d176c5666d6de814c92414d7640a798388f83b3eaed1a240ad700 = $this->env->getExtension("native_profiler");
        $__internal_9535dc597c8d176c5666d6de814c92414d7640a798388f83b3eaed1a240ad700->enter($__internal_9535dc597c8d176c5666d6de814c92414d7640a798388f83b3eaed1a240ad700_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<h1>Bem-Vindo</h1>
";
        
        $__internal_9535dc597c8d176c5666d6de814c92414d7640a798388f83b3eaed1a240ad700->leave($__internal_9535dc597c8d176c5666d6de814c92414d7640a798388f83b3eaed1a240ad700_prof);

    }

    public function getTemplateName()
    {
        return "CoreBundle:Index:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends "::base.html.twig" %}*/
/* */
/* {% block title %}Clube Saudosista de Piracicaba{% endblock %}*/
/* */
/* {% block body %}*/
/* <h1>Bem-Vindo</h1>*/
/* {% endblock %}*/
/* */
