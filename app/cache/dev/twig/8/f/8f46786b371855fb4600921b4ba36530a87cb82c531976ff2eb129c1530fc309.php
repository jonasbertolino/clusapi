<?php

/* CoreBundle:User:new.html.twig */
class __TwigTemplate_aa6b2ce81758c99d2996603fe7e783b86e16d2c27e693307fa17f131f2e8e846 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "CoreBundle:User:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8b620180f19043ddbe3f7aa22413fe7d50ceccd2768178bb0d337e825eacd796 = $this->env->getExtension("native_profiler");
        $__internal_8b620180f19043ddbe3f7aa22413fe7d50ceccd2768178bb0d337e825eacd796->enter($__internal_8b620180f19043ddbe3f7aa22413fe7d50ceccd2768178bb0d337e825eacd796_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CoreBundle:User:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8b620180f19043ddbe3f7aa22413fe7d50ceccd2768178bb0d337e825eacd796->leave($__internal_8b620180f19043ddbe3f7aa22413fe7d50ceccd2768178bb0d337e825eacd796_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_aba99b56901cb271d9bebe78d47b25373ee6d01835544a36582478db51b0b3d8 = $this->env->getExtension("native_profiler");
        $__internal_aba99b56901cb271d9bebe78d47b25373ee6d01835544a36582478db51b0b3d8->enter($__internal_aba99b56901cb271d9bebe78d47b25373ee6d01835544a36582478db51b0b3d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>User creation</h1>

    ";
        // line 6
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("usuario");
        echo "\">
            Back to the list
        </a>
    </li>
</ul>
";
        
        $__internal_aba99b56901cb271d9bebe78d47b25373ee6d01835544a36582478db51b0b3d8->leave($__internal_aba99b56901cb271d9bebe78d47b25373ee6d01835544a36582478db51b0b3d8_prof);

    }

    public function getTemplateName()
    {
        return "CoreBundle:User:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 10,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     <h1>User creation</h1>*/
/* */
/*     {{ form(form) }}*/
/* */
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('usuario') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/* </ul>*/
/* {% endblock %}*/
/* */
