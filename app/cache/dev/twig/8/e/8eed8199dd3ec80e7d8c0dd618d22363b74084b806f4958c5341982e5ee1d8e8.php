<?php

/* CoreBundle:Evento:new.html.twig */
class __TwigTemplate_0852d40d64523c7d614f0ac5309355b4d93425eea4890c2efb140dc53940c207 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "CoreBundle:Evento:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b00a4f1271e48f15f230b76dea74f5217d93a28423baa931c4154ce6f22209a1 = $this->env->getExtension("native_profiler");
        $__internal_b00a4f1271e48f15f230b76dea74f5217d93a28423baa931c4154ce6f22209a1->enter($__internal_b00a4f1271e48f15f230b76dea74f5217d93a28423baa931c4154ce6f22209a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CoreBundle:Evento:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b00a4f1271e48f15f230b76dea74f5217d93a28423baa931c4154ce6f22209a1->leave($__internal_b00a4f1271e48f15f230b76dea74f5217d93a28423baa931c4154ce6f22209a1_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_be17761a6c9e767008b9751e13856cd6690f1bd494ebb025d441a1bd66a1e6a1 = $this->env->getExtension("native_profiler");
        $__internal_be17761a6c9e767008b9751e13856cd6690f1bd494ebb025d441a1bd66a1e6a1->enter($__internal_be17761a6c9e767008b9751e13856cd6690f1bd494ebb025d441a1bd66a1e6a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>Evento creation</h1>

    ";
        // line 6
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("evento");
        echo "\">
            Back to the list
        </a>
    </li>
</ul>
";
        
        $__internal_be17761a6c9e767008b9751e13856cd6690f1bd494ebb025d441a1bd66a1e6a1->leave($__internal_be17761a6c9e767008b9751e13856cd6690f1bd494ebb025d441a1bd66a1e6a1_prof);

    }

    public function getTemplateName()
    {
        return "CoreBundle:Evento:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 10,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     <h1>Evento creation</h1>*/
/* */
/*     {{ form(form) }}*/
/* */
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('evento') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/* </ul>*/
/* {% endblock %}*/
/* */
