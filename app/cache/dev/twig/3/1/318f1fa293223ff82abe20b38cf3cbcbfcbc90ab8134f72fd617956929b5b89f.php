<?php

/* CoreBundle:Evento:edit.html.twig */
class __TwigTemplate_55d52b5af9c022c3b454ef7086e80a17785f87d28bf48c2d2e89a8c61ee15bdb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "CoreBundle:Evento:edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_794320eac10b447b1ae86711112a49118195a51f689760a5637f73642a69223a = $this->env->getExtension("native_profiler");
        $__internal_794320eac10b447b1ae86711112a49118195a51f689760a5637f73642a69223a->enter($__internal_794320eac10b447b1ae86711112a49118195a51f689760a5637f73642a69223a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CoreBundle:Evento:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_794320eac10b447b1ae86711112a49118195a51f689760a5637f73642a69223a->leave($__internal_794320eac10b447b1ae86711112a49118195a51f689760a5637f73642a69223a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_cff15a65a7eb85bf1ae01c3a4f91ba3c973281fb1f1c4b5eee3cdeaf189518c5 = $this->env->getExtension("native_profiler");
        $__internal_cff15a65a7eb85bf1ae01c3a4f91ba3c973281fb1f1c4b5eee3cdeaf189518c5->enter($__internal_cff15a65a7eb85bf1ae01c3a4f91ba3c973281fb1f1c4b5eee3cdeaf189518c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>Evento edit</h1>

    ";
        // line 6
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form');
        echo "

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("evento");
        echo "\">
            Back to the list
        </a>
    </li>
    <li>";
        // line 14
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form');
        echo "</li>
</ul>
";
        
        $__internal_cff15a65a7eb85bf1ae01c3a4f91ba3c973281fb1f1c4b5eee3cdeaf189518c5->leave($__internal_cff15a65a7eb85bf1ae01c3a4f91ba3c973281fb1f1c4b5eee3cdeaf189518c5_prof);

    }

    public function getTemplateName()
    {
        return "CoreBundle:Evento:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 14,  51 => 10,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     <h1>Evento edit</h1>*/
/* */
/*     {{ form(edit_form) }}*/
/* */
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('evento') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/*     <li>{{ form(delete_form) }}</li>*/
/* </ul>*/
/* {% endblock %}*/
/* */
