<?php

/* CoreBundle:Evento:index.html.twig */
class __TwigTemplate_0ecace77290d0d89c33781fba1e95fb1557e69f90ca40587ac83f022054ab62c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "CoreBundle:Evento:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2efedbae28496c304be55bb45c27cb0c0629c44c49d06cc024ccc23d20301644 = $this->env->getExtension("native_profiler");
        $__internal_2efedbae28496c304be55bb45c27cb0c0629c44c49d06cc024ccc23d20301644->enter($__internal_2efedbae28496c304be55bb45c27cb0c0629c44c49d06cc024ccc23d20301644_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CoreBundle:Evento:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2efedbae28496c304be55bb45c27cb0c0629c44c49d06cc024ccc23d20301644->leave($__internal_2efedbae28496c304be55bb45c27cb0c0629c44c49d06cc024ccc23d20301644_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_ff5f7c49248d05a1302062500a2c21e7fe02c47d262a8ef5d8bec2eb0d312f06 = $this->env->getExtension("native_profiler");
        $__internal_ff5f7c49248d05a1302062500a2c21e7fe02c47d262a8ef5d8bec2eb0d312f06->enter($__internal_ff5f7c49248d05a1302062500a2c21e7fe02c47d262a8ef5d8bec2eb0d312f06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>Evento list</h1>

    <table class=\"records_list\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nome</th>
                <th>Data</th>
                <th>Atracao</th>
                <th>Local</th>
                <th>Precocadeira</th>
                <th>Precoingresso</th>
                <th>Createdat</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 22
            echo "            <tr>
                <td><a href=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("evento_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "nome", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 25
            if ($this->getAttribute($context["entity"], "data", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "data", array()), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "atracao", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "local", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "precoCadeira", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "precoIngresso", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 30
            if ($this->getAttribute($context["entity"], "createdAt", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "createdAt", array()), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>
                <ul>
                    <li>
                        <a href=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("evento_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">show</a>
                    </li>
                    <li>
                        <a href=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("evento_comprar", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">comprar</a>
                    </li>
                    <li>
                        <a href=\"";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("evento_edit", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">edit</a>
                    </li>
                </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 46
        echo "        </tbody>
    </table>

        <ul>
        <li>
            <a href=\"";
        // line 51
        echo $this->env->getExtension('routing')->getPath("evento_new");
        echo "\">
                Create a new entry
            </a>
        </li>
    </ul>
    ";
        
        $__internal_ff5f7c49248d05a1302062500a2c21e7fe02c47d262a8ef5d8bec2eb0d312f06->leave($__internal_ff5f7c49248d05a1302062500a2c21e7fe02c47d262a8ef5d8bec2eb0d312f06_prof);

    }

    public function getTemplateName()
    {
        return "CoreBundle:Evento:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 51,  131 => 46,  119 => 40,  113 => 37,  107 => 34,  98 => 30,  94 => 29,  90 => 28,  86 => 27,  82 => 26,  76 => 25,  72 => 24,  66 => 23,  63 => 22,  59 => 21,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     <h1>Evento list</h1>*/
/* */
/*     <table class="records_list">*/
/*         <thead>*/
/*             <tr>*/
/*                 <th>Id</th>*/
/*                 <th>Nome</th>*/
/*                 <th>Data</th>*/
/*                 <th>Atracao</th>*/
/*                 <th>Local</th>*/
/*                 <th>Precocadeira</th>*/
/*                 <th>Precoingresso</th>*/
/*                 <th>Createdat</th>*/
/*                 <th>Actions</th>*/
/*             </tr>*/
/*         </thead>*/
/*         <tbody>*/
/*         {% for entity in entities %}*/
/*             <tr>*/
/*                 <td><a href="{{ path('evento_show', { 'id': entity.id }) }}">{{ entity.id }}</a></td>*/
/*                 <td>{{ entity.nome }}</td>*/
/*                 <td>{% if entity.data %}{{ entity.data|date('Y-m-d H:i:s') }}{% endif %}</td>*/
/*                 <td>{{ entity.atracao }}</td>*/
/*                 <td>{{ entity.local }}</td>*/
/*                 <td>{{ entity.precoCadeira }}</td>*/
/*                 <td>{{ entity.precoIngresso }}</td>*/
/*                 <td>{% if entity.createdAt %}{{ entity.createdAt|date('Y-m-d H:i:s') }}{% endif %}</td>*/
/*                 <td>*/
/*                 <ul>*/
/*                     <li>*/
/*                         <a href="{{ path('evento_show', { 'id': entity.id }) }}">show</a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="{{ path('evento_comprar', { 'id': entity.id }) }}">comprar</a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="{{ path('evento_edit', { 'id': entity.id }) }}">edit</a>*/
/*                     </li>*/
/*                 </ul>*/
/*                 </td>*/
/*             </tr>*/
/*         {% endfor %}*/
/*         </tbody>*/
/*     </table>*/
/* */
/*         <ul>*/
/*         <li>*/
/*             <a href="{{ path('evento_new') }}">*/
/*                 Create a new entry*/
/*             </a>*/
/*         </li>*/
/*     </ul>*/
/*     {% endblock %}*/
/* */
