<?php

/* ::base.html.twig */
class __TwigTemplate_46169c8308a5e60702e64da42510da59a8c2feb37dda142001069bb3dd4fe393 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'javascripts' => array($this, 'block_javascripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'script' => array($this, 'block_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_63131618ad399cd0933413d5b574f4d439b091cb2f3bad9792aa0538dd00e717 = $this->env->getExtension("native_profiler");
        $__internal_63131618ad399cd0933413d5b574f4d439b091cb2f3bad9792aa0538dd00e717->enter($__internal_63131618ad399cd0933413d5b574f4d439b091cb2f3bad9792aa0538dd00e717_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

        ";
        // line 7
        $this->displayBlock('javascripts', $context, $blocks);
        // line 14
        echo "
        ";
        // line 15
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 19
        echo "        
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 23
        $this->displayBlock('body', $context, $blocks);
        // line 26
        echo "        

        ";
        // line 28
        $this->displayBlock('script', $context, $blocks);
        // line 30
        echo "    </body>
</html>
";
        
        $__internal_63131618ad399cd0933413d5b574f4d439b091cb2f3bad9792aa0538dd00e717->leave($__internal_63131618ad399cd0933413d5b574f4d439b091cb2f3bad9792aa0538dd00e717_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_b56372b17ad6508bf0b34bde135f3325007d95981f8519e0ef59545faa3b9d42 = $this->env->getExtension("native_profiler");
        $__internal_b56372b17ad6508bf0b34bde135f3325007d95981f8519e0ef59545faa3b9d42->enter($__internal_b56372b17ad6508bf0b34bde135f3325007d95981f8519e0ef59545faa3b9d42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_b56372b17ad6508bf0b34bde135f3325007d95981f8519e0ef59545faa3b9d42->leave($__internal_b56372b17ad6508bf0b34bde135f3325007d95981f8519e0ef59545faa3b9d42_prof);

    }

    // line 7
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_e060b500aa3b7b9cee7b31b182be820bb647694e21118c223e4b5b7f3ae21b65 = $this->env->getExtension("native_profiler");
        $__internal_e060b500aa3b7b9cee7b31b182be820bb647694e21118c223e4b5b7f3ae21b65->enter($__internal_e060b500aa3b7b9cee7b31b182be820bb647694e21118c223e4b5b7f3ae21b65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 8
        echo "            <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/core/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/core/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/core/js/funcoes.js"), "html", null, true);
        echo "\"></script>
            <script type=\"text/javascript\" src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/core/js/jquery.price_format.2.0.min.js"), "html", null, true);
        echo "\"></script>
            
        ";
        
        $__internal_e060b500aa3b7b9cee7b31b182be820bb647694e21118c223e4b5b7f3ae21b65->leave($__internal_e060b500aa3b7b9cee7b31b182be820bb647694e21118c223e4b5b7f3ae21b65_prof);

    }

    // line 15
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_8402084f7a310d0995abebdf1bc3ac4dafe06623f83071c4e8952113912e4c37 = $this->env->getExtension("native_profiler");
        $__internal_8402084f7a310d0995abebdf1bc3ac4dafe06623f83071c4e8952113912e4c37->enter($__internal_8402084f7a310d0995abebdf1bc3ac4dafe06623f83071c4e8952113912e4c37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 16
        echo "            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/core/css/bootstrap.min.css"), "html", null, true);
        echo "\" />
            <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/core/css/base.css"), "html", null, true);
        echo "\" />
        ";
        
        $__internal_8402084f7a310d0995abebdf1bc3ac4dafe06623f83071c4e8952113912e4c37->leave($__internal_8402084f7a310d0995abebdf1bc3ac4dafe06623f83071c4e8952113912e4c37_prof);

    }

    // line 23
    public function block_body($context, array $blocks = array())
    {
        $__internal_4cc0922270e381b44a5e323cfbe72b4b2123b227c438bd9b2416806c30b7ebba = $this->env->getExtension("native_profiler");
        $__internal_4cc0922270e381b44a5e323cfbe72b4b2123b227c438bd9b2416806c30b7ebba->enter($__internal_4cc0922270e381b44a5e323cfbe72b4b2123b227c438bd9b2416806c30b7ebba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 24
        echo "
        ";
        
        $__internal_4cc0922270e381b44a5e323cfbe72b4b2123b227c438bd9b2416806c30b7ebba->leave($__internal_4cc0922270e381b44a5e323cfbe72b4b2123b227c438bd9b2416806c30b7ebba_prof);

    }

    // line 28
    public function block_script($context, array $blocks = array())
    {
        $__internal_9d0d0639c600e2e13f629e7e730cf2c88b301e491b43679d647d08559cd7c2e1 = $this->env->getExtension("native_profiler");
        $__internal_9d0d0639c600e2e13f629e7e730cf2c88b301e491b43679d647d08559cd7c2e1->enter($__internal_9d0d0639c600e2e13f629e7e730cf2c88b301e491b43679d647d08559cd7c2e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "script"));

        // line 29
        echo "        ";
        
        $__internal_9d0d0639c600e2e13f629e7e730cf2c88b301e491b43679d647d08559cd7c2e1->leave($__internal_9d0d0639c600e2e13f629e7e730cf2c88b301e491b43679d647d08559cd7c2e1_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 29,  146 => 28,  138 => 24,  132 => 23,  123 => 17,  118 => 16,  112 => 15,  102 => 11,  98 => 10,  94 => 9,  89 => 8,  83 => 7,  71 => 5,  62 => 30,  60 => 28,  56 => 26,  54 => 23,  48 => 20,  45 => 19,  43 => 15,  40 => 14,  38 => 7,  33 => 5,  27 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/* */
/*         {% block javascripts %}*/
/*             <script type="text/javascript" src="{{ asset('bundles/core/js/jquery.min.js') }}"></script>*/
/*             <script type="text/javascript" src="{{ asset('bundles/core/js/bootstrap.min.js') }}"></script>*/
/*             <script type="text/javascript" src="{{ asset('bundles/core/js/funcoes.js') }}"></script>*/
/*             <script type="text/javascript" src="{{ asset('bundles/core/js/jquery.price_format.2.0.min.js') }}"></script>*/
/*             */
/*         {% endblock %}*/
/* */
/*         {% block stylesheets %}*/
/*             <link rel="stylesheet" type="text/css" href="{{ asset('bundles/core/css/bootstrap.min.css') }}" />*/
/*             <link rel="stylesheet" type="text/css" href="{{ asset('bundles/core/css/base.css') }}" />*/
/*         {% endblock %}*/
/*         */
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}*/
/* */
/*         {% endblock %}*/
/*         */
/* */
/*         {% block script %}*/
/*         {% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
