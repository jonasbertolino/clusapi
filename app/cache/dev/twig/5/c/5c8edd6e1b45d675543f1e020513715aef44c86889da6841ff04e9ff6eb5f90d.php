<?php

/* CoreBundle:User:edit.html.twig */
class __TwigTemplate_f8b4b8a10348dd988ed67ff6a80ad0f8e31fecb9c10d7f839f92b569dc8f072f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "CoreBundle:User:edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d3e07c7e6be5c19cf4320b4c9770d0125032ac25ad8d4a37d086ca5975b61cb0 = $this->env->getExtension("native_profiler");
        $__internal_d3e07c7e6be5c19cf4320b4c9770d0125032ac25ad8d4a37d086ca5975b61cb0->enter($__internal_d3e07c7e6be5c19cf4320b4c9770d0125032ac25ad8d4a37d086ca5975b61cb0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CoreBundle:User:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d3e07c7e6be5c19cf4320b4c9770d0125032ac25ad8d4a37d086ca5975b61cb0->leave($__internal_d3e07c7e6be5c19cf4320b4c9770d0125032ac25ad8d4a37d086ca5975b61cb0_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_eca317219a8dc720e6c729637e2f16f4e05e3f84a92abc8a518c04271cc6c573 = $this->env->getExtension("native_profiler");
        $__internal_eca317219a8dc720e6c729637e2f16f4e05e3f84a92abc8a518c04271cc6c573->enter($__internal_eca317219a8dc720e6c729637e2f16f4e05e3f84a92abc8a518c04271cc6c573_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>User edit</h1>

    ";
        // line 6
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form');
        echo "

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("usuario");
        echo "\">
            Back to the list
        </a>
    </li>
    <li>";
        // line 14
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form');
        echo "</li>
</ul>
";
        
        $__internal_eca317219a8dc720e6c729637e2f16f4e05e3f84a92abc8a518c04271cc6c573->leave($__internal_eca317219a8dc720e6c729637e2f16f4e05e3f84a92abc8a518c04271cc6c573_prof);

    }

    public function getTemplateName()
    {
        return "CoreBundle:User:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 14,  51 => 10,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     <h1>User edit</h1>*/
/* */
/*     {{ form(edit_form) }}*/
/* */
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('usuario') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/*     <li>{{ form(delete_form) }}</li>*/
/* </ul>*/
/* {% endblock %}*/
/* */
