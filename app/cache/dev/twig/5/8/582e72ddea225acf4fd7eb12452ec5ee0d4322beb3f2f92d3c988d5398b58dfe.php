<?php

/* CoreBundle:Evento:show.html.twig */
class __TwigTemplate_0861b247298d4707ea1871b09ba4690f32702acbe7d3901c2a40f60f5c02517e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "CoreBundle:Evento:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d2da4669c410081fa8be2f57b8cb0655ffc38478f011f54f2a940fb943b0967a = $this->env->getExtension("native_profiler");
        $__internal_d2da4669c410081fa8be2f57b8cb0655ffc38478f011f54f2a940fb943b0967a->enter($__internal_d2da4669c410081fa8be2f57b8cb0655ffc38478f011f54f2a940fb943b0967a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CoreBundle:Evento:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d2da4669c410081fa8be2f57b8cb0655ffc38478f011f54f2a940fb943b0967a->leave($__internal_d2da4669c410081fa8be2f57b8cb0655ffc38478f011f54f2a940fb943b0967a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_7e7c28f1fb1a5349284968e14676bc14e60fe32f02b28f639a96cc6a0aebb5db = $this->env->getExtension("native_profiler");
        $__internal_7e7c28f1fb1a5349284968e14676bc14e60fe32f02b28f639a96cc6a0aebb5db->enter($__internal_7e7c28f1fb1a5349284968e14676bc14e60fe32f02b28f639a96cc6a0aebb5db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>Evento</h1>

    <table class=\"record_properties\">
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nome</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nome", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Data</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "data", array()), "Y-m-d H:i:s"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Atracao</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "atracao", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Local</th>
                <td>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "local", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Precocadeira</th>
                <td>";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "precoCadeira", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Precoingresso</th>
                <td>";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "precoIngresso", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Createdat</th>
                <td>";
        // line 38
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "createdAt", array()), "Y-m-d H:i:s"), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 45
        echo $this->env->getExtension('routing')->getPath("evento");
        echo "\">
            Back to the list
        </a>
    </li>
    <li>
        <a href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("evento_edit", array("id" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "id", array()))), "html", null, true);
        echo "\">
            Edit
        </a>
    </li>
    <li>";
        // line 54
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form');
        echo "</li>
</ul>
";
        
        $__internal_7e7c28f1fb1a5349284968e14676bc14e60fe32f02b28f639a96cc6a0aebb5db->leave($__internal_7e7c28f1fb1a5349284968e14676bc14e60fe32f02b28f639a96cc6a0aebb5db_prof);

    }

    public function getTemplateName()
    {
        return "CoreBundle:Evento:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 54,  115 => 50,  107 => 45,  97 => 38,  90 => 34,  83 => 30,  76 => 26,  69 => 22,  62 => 18,  55 => 14,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     <h1>Evento</h1>*/
/* */
/*     <table class="record_properties">*/
/*         <tbody>*/
/*             <tr>*/
/*                 <th>Id</th>*/
/*                 <td>{{ entity.id }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Nome</th>*/
/*                 <td>{{ entity.nome }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Data</th>*/
/*                 <td>{{ entity.data|date('Y-m-d H:i:s') }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Atracao</th>*/
/*                 <td>{{ entity.atracao }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Local</th>*/
/*                 <td>{{ entity.local }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Precocadeira</th>*/
/*                 <td>{{ entity.precoCadeira }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Precoingresso</th>*/
/*                 <td>{{ entity.precoIngresso }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Createdat</th>*/
/*                 <td>{{ entity.createdAt|date('Y-m-d H:i:s') }}</td>*/
/*             </tr>*/
/*         </tbody>*/
/*     </table>*/
/* */
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('evento') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/*     <li>*/
/*         <a href="{{ path('evento_edit', { 'id': entity.id }) }}">*/
/*             Edit*/
/*         </a>*/
/*     </li>*/
/*     <li>{{ form(delete_form) }}</li>*/
/* </ul>*/
/* {% endblock %}*/
/* */
