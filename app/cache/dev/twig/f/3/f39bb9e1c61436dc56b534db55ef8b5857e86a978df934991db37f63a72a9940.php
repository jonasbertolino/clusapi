<?php

/* CoreBundle:User:index.html.twig */
class __TwigTemplate_946b67480a79d210b816e716798ea7812b452a170bd7e50c5cc9c2b004801385 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "CoreBundle:User:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_92fe316fa0f5e3e543a896759b54f95d45394292190c0f17e0da409cd3cea15f = $this->env->getExtension("native_profiler");
        $__internal_92fe316fa0f5e3e543a896759b54f95d45394292190c0f17e0da409cd3cea15f->enter($__internal_92fe316fa0f5e3e543a896759b54f95d45394292190c0f17e0da409cd3cea15f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CoreBundle:User:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_92fe316fa0f5e3e543a896759b54f95d45394292190c0f17e0da409cd3cea15f->leave($__internal_92fe316fa0f5e3e543a896759b54f95d45394292190c0f17e0da409cd3cea15f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_af21446db96c43d2bad015bb8acb29c56fcd496ce8e6623f8c6848228cd5a1cb = $this->env->getExtension("native_profiler");
        $__internal_af21446db96c43d2bad015bb8acb29c56fcd496ce8e6623f8c6848228cd5a1cb->enter($__internal_af21446db96c43d2bad015bb8acb29c56fcd496ce8e6623f8c6848228cd5a1cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>User list</h1>

    <table class=\"records_list\">
        <thead>
            <tr>
                <th>Email</th>
                <th>Cpf</th>
                <th>Rg</th>
                <th>Nome</th>
                <th>Datanascimento</th>
                <th>Sexo</th>
                <th>Ddd</th>
                <th>Telefone</th>
                <th>Cep</th>
                <th>Logradouro</th>
                <th>Numero</th>
                <th>Complemento</th>
                <th>Bairro</th>
                <th>Cidade</th>
                <th>Uf</th>
                <th>Newsletter</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 30
            echo "            <tr>
                <td><a href=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("usuario_show", array("email" => $this->getAttribute($context["entity"], "email", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "email", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "cpf", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "rg", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "nome", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 35
            if ($this->getAttribute($context["entity"], "dataNascimento", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "dataNascimento", array()), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "sexo", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "ddd", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "telefone", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "cep", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 40
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "logradouro", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "numero", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "complemento", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "bairro", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "cidade", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "uf", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "newsletter", array()), "html", null, true);
            echo "</td>
                <td>
                <ul>
                    <li>
                        <a href=\"";
            // line 50
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("usuario_show", array("email" => $this->getAttribute($context["entity"], "email", array()))), "html", null, true);
            echo "\">show</a>
                    </li>
                    <li>
                        <a href=\"";
            // line 53
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("usuario_edit", array("email" => $this->getAttribute($context["entity"], "email", array()))), "html", null, true);
            echo "\">edit</a>
                    </li>
                </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "        </tbody>
    </table>

        <ul>
        <li>
            <a href=\"";
        // line 64
        echo $this->env->getExtension('routing')->getPath("usuario_new");
        echo "\">
                Create a new entry
            </a>
        </li>
    </ul>
    ";
        
        $__internal_af21446db96c43d2bad015bb8acb29c56fcd496ce8e6623f8c6848228cd5a1cb->leave($__internal_af21446db96c43d2bad015bb8acb29c56fcd496ce8e6623f8c6848228cd5a1cb_prof);

    }

    public function getTemplateName()
    {
        return "CoreBundle:User:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 64,  163 => 59,  151 => 53,  145 => 50,  138 => 46,  134 => 45,  130 => 44,  126 => 43,  122 => 42,  118 => 41,  114 => 40,  110 => 39,  106 => 38,  102 => 37,  98 => 36,  92 => 35,  88 => 34,  84 => 33,  80 => 32,  74 => 31,  71 => 30,  67 => 29,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     <h1>User list</h1>*/
/* */
/*     <table class="records_list">*/
/*         <thead>*/
/*             <tr>*/
/*                 <th>Email</th>*/
/*                 <th>Cpf</th>*/
/*                 <th>Rg</th>*/
/*                 <th>Nome</th>*/
/*                 <th>Datanascimento</th>*/
/*                 <th>Sexo</th>*/
/*                 <th>Ddd</th>*/
/*                 <th>Telefone</th>*/
/*                 <th>Cep</th>*/
/*                 <th>Logradouro</th>*/
/*                 <th>Numero</th>*/
/*                 <th>Complemento</th>*/
/*                 <th>Bairro</th>*/
/*                 <th>Cidade</th>*/
/*                 <th>Uf</th>*/
/*                 <th>Newsletter</th>*/
/*                 <th>Actions</th>*/
/*             </tr>*/
/*         </thead>*/
/*         <tbody>*/
/*         {% for entity in entities %}*/
/*             <tr>*/
/*                 <td><a href="{{ path('usuario_show', { 'email': entity.email }) }}">{{ entity.email }}</a></td>*/
/*                 <td>{{ entity.cpf }}</td>*/
/*                 <td>{{ entity.rg }}</td>*/
/*                 <td>{{ entity.nome }}</td>*/
/*                 <td>{% if entity.dataNascimento %}{{ entity.dataNascimento|date('Y-m-d H:i:s') }}{% endif %}</td>*/
/*                 <td>{{ entity.sexo }}</td>*/
/*                 <td>{{ entity.ddd }}</td>*/
/*                 <td>{{ entity.telefone }}</td>*/
/*                 <td>{{ entity.cep }}</td>*/
/*                 <td>{{ entity.logradouro }}</td>*/
/*                 <td>{{ entity.numero }}</td>*/
/*                 <td>{{ entity.complemento }}</td>*/
/*                 <td>{{ entity.bairro }}</td>*/
/*                 <td>{{ entity.cidade }}</td>*/
/*                 <td>{{ entity.uf }}</td>*/
/*                 <td>{{ entity.newsletter }}</td>*/
/*                 <td>*/
/*                 <ul>*/
/*                     <li>*/
/*                         <a href="{{ path('usuario_show', { 'email': entity.email }) }}">show</a>*/
/*                     </li>*/
/*                     <li>*/
/*                         <a href="{{ path('usuario_edit', { 'email': entity.email }) }}">edit</a>*/
/*                     </li>*/
/*                 </ul>*/
/*                 </td>*/
/*             </tr>*/
/*         {% endfor %}*/
/*         </tbody>*/
/*     </table>*/
/* */
/*         <ul>*/
/*         <li>*/
/*             <a href="{{ path('usuario_new') }}">*/
/*                 Create a new entry*/
/*             </a>*/
/*         </li>*/
/*     </ul>*/
/*     {% endblock %}*/
/* */
