<?php

/* CoreBundle:User:show.html.twig */
class __TwigTemplate_2f33af56136ed4957e8a9f0f2dbddcb63a77839f20e1de9bfd8ae96af8469488 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "CoreBundle:User:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a0f10c0ed98e83b12c0594c52371ac427b3fa18e6ec5f8ba0ef7abc6abe53960 = $this->env->getExtension("native_profiler");
        $__internal_a0f10c0ed98e83b12c0594c52371ac427b3fa18e6ec5f8ba0ef7abc6abe53960->enter($__internal_a0f10c0ed98e83b12c0594c52371ac427b3fa18e6ec5f8ba0ef7abc6abe53960_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CoreBundle:User:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a0f10c0ed98e83b12c0594c52371ac427b3fa18e6ec5f8ba0ef7abc6abe53960->leave($__internal_a0f10c0ed98e83b12c0594c52371ac427b3fa18e6ec5f8ba0ef7abc6abe53960_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_a73c21cb3f2ac503ceeb13cb4d445efaf55234854d58ae0f34d5b18757d27a0a = $this->env->getExtension("native_profiler");
        $__internal_a73c21cb3f2ac503ceeb13cb4d445efaf55234854d58ae0f34d5b18757d27a0a->enter($__internal_a73c21cb3f2ac503ceeb13cb4d445efaf55234854d58ae0f34d5b18757d27a0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h1>User</h1>

    <table class=\"record_properties\">
        <tbody>
            <tr>
                <th>Email</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "email", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Cpf</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "cpf", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Rg</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "rg", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nome</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "nome", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Datanascimento</th>
                <td>";
        // line 26
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "dataNascimento", array()), "Y-m-d H:i:s"), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Sexo</th>
                <td>";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "sexo", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Ddd</th>
                <td>";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "ddd", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Telefone</th>
                <td>";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "telefone", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Cep</th>
                <td>";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "cep", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Logradouro</th>
                <td>";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "logradouro", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Numero</th>
                <td>";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "numero", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Complemento</th>
                <td>";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "complemento", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Bairro</th>
                <td>";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "bairro", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Cidade</th>
                <td>";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "cidade", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Uf</th>
                <td>";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "uf", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Newsletter</th>
                <td>";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "newsletter", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

        <ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 77
        echo $this->env->getExtension('routing')->getPath("usuario");
        echo "\">
            Back to the list
        </a>
    </li>
    <li>
        <a href=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("usuario_edit", array("email" => $this->getAttribute((isset($context["entity"]) ? $context["entity"] : $this->getContext($context, "entity")), "email", array()))), "html", null, true);
        echo "\">
            Edit
        </a>
    </li>
    <li>";
        // line 86
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form');
        echo "</li>
</ul>
";
        
        $__internal_a73c21cb3f2ac503ceeb13cb4d445efaf55234854d58ae0f34d5b18757d27a0a->leave($__internal_a73c21cb3f2ac503ceeb13cb4d445efaf55234854d58ae0f34d5b18757d27a0a_prof);

    }

    public function getTemplateName()
    {
        return "CoreBundle:User:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 86,  171 => 82,  163 => 77,  153 => 70,  146 => 66,  139 => 62,  132 => 58,  125 => 54,  118 => 50,  111 => 46,  104 => 42,  97 => 38,  90 => 34,  83 => 30,  76 => 26,  69 => 22,  62 => 18,  55 => 14,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     <h1>User</h1>*/
/* */
/*     <table class="record_properties">*/
/*         <tbody>*/
/*             <tr>*/
/*                 <th>Email</th>*/
/*                 <td>{{ entity.email }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Cpf</th>*/
/*                 <td>{{ entity.cpf }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Rg</th>*/
/*                 <td>{{ entity.rg }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Nome</th>*/
/*                 <td>{{ entity.nome }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Datanascimento</th>*/
/*                 <td>{{ entity.dataNascimento|date('Y-m-d H:i:s') }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Sexo</th>*/
/*                 <td>{{ entity.sexo }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Ddd</th>*/
/*                 <td>{{ entity.ddd }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Telefone</th>*/
/*                 <td>{{ entity.telefone }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Cep</th>*/
/*                 <td>{{ entity.cep }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Logradouro</th>*/
/*                 <td>{{ entity.logradouro }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Numero</th>*/
/*                 <td>{{ entity.numero }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Complemento</th>*/
/*                 <td>{{ entity.complemento }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Bairro</th>*/
/*                 <td>{{ entity.bairro }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Cidade</th>*/
/*                 <td>{{ entity.cidade }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Uf</th>*/
/*                 <td>{{ entity.uf }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Newsletter</th>*/
/*                 <td>{{ entity.newsletter }}</td>*/
/*             </tr>*/
/*         </tbody>*/
/*     </table>*/
/* */
/*         <ul class="record_actions">*/
/*     <li>*/
/*         <a href="{{ path('usuario') }}">*/
/*             Back to the list*/
/*         </a>*/
/*     </li>*/
/*     <li>*/
/*         <a href="{{ path('usuario_edit', { 'email': entity.email }) }}">*/
/*             Edit*/
/*         </a>*/
/*     </li>*/
/*     <li>{{ form(delete_form) }}</li>*/
/* </ul>*/
/* {% endblock %}*/
/* */
