<?php

namespace JJB\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Mesa
 *
 * @ORM\Table(name="mesa")
 * @ORM\Entity(repositoryClass="JJB\CoreBundle\Entity\MesaRepository")
 */
class Mesa
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="numeroMesa", type="integer")
     * @Assert\NotBlank
     */
    private $numeroMesa;

    /**
     * @var boolean
     *
     * @ORM\Column(name="comprada", type="boolean", options={"default":false})
     */
    private $comprada;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Evento")
     * @ORM\Id
     * @ORM\JoinColumn(name="evento_id", referencedColumnName="id", nullable=false)
     */    
    private $evento;

    /**
     * Set numeroMesa
     *
     * @param integer $numeroMesa
     *
     * @return Mesa
     */
    public function setNumeroMesa($numeroMesa)
    {
        $this->numeroMesa = $numeroMesa;

        return $this;
    }

    /**
     * Get numeroMesa
     *
     * @return integer
     */
    public function getNumeroMesa()
    {
        return $this->numeroMesa;
    }

    /**
     * Set comprada
     *
     * @param boolean $comprada
     *
     * @return Mesa
     */
    public function setComprada($comprada)
    {
        $this->comprada = $comprada;

        return $this;
    }

    /**
     * Get comprada
     *
     * @return boolean
     */
    public function getComprada()
    {
        return $this->comprada;
    }

    /**
     * Set evento
     *
     * @param \JJB\CoreBundle\Entity\Evento $evento
     *
     * @return Mesa
     */
    public function setEvento(\JJB\CoreBundle\Entity\Evento $evento)
    {
        $this->evento = $evento;

        return $this;
    }

    /**
     * Get evento
     *
     * @return \JJB\CoreBundle\Entity\Evento
     */
    public function getEvento()
    {
        return $this->evento;
    }
}
