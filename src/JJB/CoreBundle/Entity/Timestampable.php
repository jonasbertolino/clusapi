<?php

namespace JJB\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

abstract class Timestampable{

	/**
	 *
	 * @var \DateTime
	 * @ORM\Column(name="created_at", type="datetime")
	 */

	private $createdAt;

	/**
	 *
	 * @var \DateTime
	 * @ORM\Column(name="update_at", type="datetime")
	 */

	private $updatedAt;
	
	 /** 
     * Construct 
     */ 
    public function __construct() 
    { 
        $this->createdAt = new \DateTime(); 
        $this->updatedAt = new \DateTime(); 
    }     

    /** 
     * Set createdAt 
     * 
     * @param $createdAt 
     * @return Timestampable
     */ 
    public function setCreatedAt($createdAt) 
    { 
        $this->createdAt = $createdAt; 

        return $this;
    } 
 
    /** 
     * Get CreatedAt 
     * 
     * @return \DateTime 
     */ 
    public function getCreatedAt() 
    { 
        return $this->createdAt; 
    } 
 
    /** 
     * Set UpdatedAt 
     * 
     * @param \DateTime $updatedAt 
     */ 
    public function setUpdatedAt($updatedAt) 
    { 
        $this->updatedAt = $updatedAt; 

        return $this;
    } 
    /** 
     * Get UpdateAt 
     * 
     * @return \DateTime 
     */ 
    public function getUpdatedAt() 
    { 
        return $this->updatedAt; 
    } 
}