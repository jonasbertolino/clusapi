<?php

namespace JJB\CoreBundle\Entity;

use JJB\CoreBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Evento
 *
 * @ORM\Table(name="evento")
 * @ORM\Entity(repositoryClass="JJB\CoreBundle\Repository\EventoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Evento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=100)
     * @Assert\NotBlank
     */
    private $nome;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data", type="datetime")
     * @Assert\NotBlank
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="atracao", type="string", length=255)
     * @Assert\NotBlank
     */
    private $atracao;

    /**
     * @var string
     *
     * @ORM\Column(name="local", type="string", length=255)
     * @Assert\NotBlank
     */
    private $local;

    /**
     * @var float
     *
     * @ORM\Column(name="precoCadeira", type="float")
     * @Assert\NotBlank
     */
    private $precoCadeira;

    /**
     * @var string
     *
     * @ORM\Column(name="precoIngresso", type="decimal")
     * @Assert\NotBlank
     */
    private $precoIngresso;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="eventos")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="email", nullable=false)
     */
    
    private $user;

    /**
     *
     * @ORM\OneToMany(targetEntity="Mesa", mappedBy="evento", cascade={"persist"})
     *
     */
    private $mesa;

    /**
     *
     * @ORM\Column(name="ativo", type="boolean", nullable=false, options={"default":1})
     *
     */
    private $ativo;
    


    public function __construct(){
        $this->createdAt = new \Datetime();
        $this->ativo = true;
        $this->mesa = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Evento
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Evento
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set atracao
     *
     * @param string $atracao
     *
     * @return Evento
     */
    public function setAtracao($atracao)
    {
        $this->atracao = $atracao;

        return $this;
    }

    /**
     * Get atracao
     *
     * @return string
     */
    public function getAtracao()
    {
        return $this->atracao;
    }

    /**
     * Set local
     *
     * @param string $local
     *
     * @return Evento
     */
    public function setLocal($local)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Get local
     *
     * @return string
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * Set precoCadeira
     *
     * @param float $precoCadeira
     *
     * @return Evento
     */
    public function setPrecoCadeira($precoCadeira)
    {
        $this->precoCadeira = $precoCadeira;

        return $this;
    }

    /**
     * Get precoCadeira
     *
     * @return float
     */
    public function getPrecoCadeira()
    {
        return $this->precoCadeira;
    }

    /**
     * Set precoIngresso
     *
     * @param string $precoIngresso
     *
     * @return Evento
     */
    public function setPrecoIngresso($precoIngresso)
    {
        $this->precoIngresso = $precoIngresso;

        return $this;
    }

    /**
     * Get precoIngresso
     *
     * @return string
     */
    public function getPrecoIngresso()
    {
        return $this->precoIngresso;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Evento
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \JJB\CoreBundle\Entity\User $user
     *
     * @return Evento
     */
    public function setUser(\JJB\CoreBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \JJB\CoreBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add mesa
     *
     * @param \JJB\CoreBundle\Entity\Mesa $mesa
     *
     * @return Evento
     */
    public function addMesa(\JJB\CoreBundle\Entity\Mesa $mesa)
    {
        $this->mesa[] = $mesa;

        return $this;
    }

    /**
     * Remove mesa
     *
     * @param \JJB\CoreBundle\Entity\Mesa $mesa
     */
    public function removeMesa(\JJB\CoreBundle\Entity\Mesa $mesa)
    {
        $this->mesa->removeElement($mesa);
    }

    /**
     * Get mesa
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMesa()
    {
        return $this->mesa;
    }

    public function __toString(){
        return $this->nome;
    }

    /**
     *
     * @ORM\PostPersist
     *
     */

    public function postInserted($args){

        $em = $args->getEntityManager();

        for ($i = 1; $i <= 2; $i++){
            $mesa = new Mesa();
            $mesa->setNumeroMesa($i);
            $mesa->setComprada(false);
            $mesa->setEvento($this);
            $em->persist($mesa);
        }

        $em->flush();
    }
    

    /**
     * Set ativo
     *
     * @param boolean $ativo
     *
     * @return Evento
     */
    public function setAtivo($ativo = true)
    {
        if(is_null($ativo)) $ativo = true;
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get ativo
     *
     * @return boolean
     */
    public function getAtivo()
    {
        return $this->ativo;
    }
}
